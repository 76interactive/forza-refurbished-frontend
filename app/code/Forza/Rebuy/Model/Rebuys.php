<?php
namespace Forza\Rebuy\Model;

class Rebuys extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'forza_rebuy_rebuys';

    protected $_cacheTag = 'forza_rebuy_rebuys';

    protected $_eventPrefix = 'forza_rebuy_rebuys';

    protected function _construct()
    {
        $this->_init('Forza\Rebuy\Model\ResourceModel\Rebuys');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    public function getCollection()
    {
        return parent::getCollection()->setPageSize(1);
    }
}