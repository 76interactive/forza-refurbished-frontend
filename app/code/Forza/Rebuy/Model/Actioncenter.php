<?php
namespace Forza\Rebuy\Model;

class Actioncenter extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'forza_rebuy_actioncenter';

    protected $_cacheTag = 'forza_rebuy_actioncenter';

    protected $_eventPrefix = 'forza_rebuy_actioncenter';

    protected function _construct()
    {
        $this->_init('Forza\Rebuy\Model\ResourceModel\Actioncenter');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}