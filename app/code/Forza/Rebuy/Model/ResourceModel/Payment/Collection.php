<?php
namespace Forza\Rebuy\Model\ResourceModel\Payment;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'forza_rebuy_payment_collection';
    protected $_eventObject = 'payment_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Forza\Rebuy\Model\Payment', 'Forza\Rebuy\Model\ResourceModel\Payment');
    }

    public function getCollection()
    {
        die("KDB");
    }
}