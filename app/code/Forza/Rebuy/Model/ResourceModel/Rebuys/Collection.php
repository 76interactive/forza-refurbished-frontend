<?php
namespace Forza\Rebuy\Model\ResourceModel\Rebuys;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'forza_rebuy_rebuys_collection';
    protected $_eventObject = 'rebuys_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Forza\Rebuy\Model\Rebuys', 'Forza\Rebuy\Model\ResourceModel\Rebuys');
    }

    public function getCollection()
    {
        die("KDB");
    }
}