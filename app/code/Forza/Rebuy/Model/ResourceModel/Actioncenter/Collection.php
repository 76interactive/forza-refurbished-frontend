<?php
namespace Forza\Rebuy\Model\ResourceModel\Actioncenter;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'forza_rebuy_actioncenter_collection';
    protected $_eventObject = 'actioncenter_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Forza\Rebuy\Model\Actioncenter', 'Forza\Rebuy\Model\ResourceModel\Actioncenter');
    }
}