<?php
namespace Forza\Rebuy\Controller\Adminhtml\Actioncenter;


class Index extends \Magento\Backend\App\Action {

    protected $actioncenterFactory;

    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Forza\Rebuy\Model\ActioncenterFactory $actioncenterFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->actioncenterFactory = $actioncenterFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $rebuys = $this->actioncenterFactory->create();
        $collection = $rebuys->getCollection()->addFieldToFilter('is_done', 0);
        foreach($collection as $item){
           echo "<pre>";
           print_r($item->getData());
           echo "</pre>";
        }
        exit();


        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Rebuys')));

        return $resultPage;
    }

    public function indexAction() {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('rebuy/adminhtml_action'));
        $this->renderLayout();
    }

    public function completeAction() {
        if ($id = $this->request->getParam('id')) {
            $model = $this->rebuyEntityRebuyActionFactory->create()->load($id);
            if($model->getId() > 0){
                $model->setIsDone(1)
                    ->save();
            }
        }
        echo $this->backendHelper->getUrl('*/*/undo/') .'id/'.$id;die;
    }


    public function undoAction() {
        if ($id = $this->request->getParam('id')) {
            $model = $this->rebuyEntityRebuyActionFactory->create()->load($id);
            if($model->getId() > 0){
                $model->setIsDone(0)
                    ->save();
            }
        }
    }

    /*
    public function _isAllowed()
    {
        return $this->backendAuthSession->isAllowed('rebuy/rebuy_action');
    }
    */
}