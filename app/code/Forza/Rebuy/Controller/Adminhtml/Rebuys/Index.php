<?php
namespace Forza\Rebuy\Controller\Adminhtml\Rebuys;


class Index extends \Magento\Backend\App\Action {

    /**
     * @var \Forza\Rebuy\Model\Entity\RebuyFactory
     */
    protected $rebuyEntityRebuyFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Framework\Session\Generic
     */
    protected $generic;

    /**
     * @var \Forza\Rebuy\Model\Entity\RebuyCommentFactory
     */
    protected $rebuyEntityRebuyCommentFactory;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $backendAuthSession;

    /**
     * @var \Forza\Rebuy\Model\Entity\RebuyProductFactory
     */
    protected $rebuyEntityRebuyProductFactory;

    /**
     * @var \Forza\Rebuy\Model\Pdf\InvoiceFactory
     */
    protected $rebuyPdfInvoiceFactory;

    /**
     * @var \Forza\Rebuy\Model\Pdf\EnquiryFactory
     */
    protected $rebuyPdfEnquiryFactory;

    /**
     * @var \Forza\Rebuy\Model\Pdf\ListFactory
     */
    protected $rebuyPdfListFactory;

    /**
     * @var \Forza\Rebuy\Model\Service\ShipmentFactory
     */
    protected $rebuyServiceShipmentFactory;

    /**
     * @var \Forza\Rebuy\Model\Service\ReturnShipmentFactory
     */
    protected $rebuyServiceReturnShipmentFactory;

    /**
     * @var \Magento\Email\Model\TemplateFactory
     */
    protected $emailTemplateFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $backendHelper;

    protected $resultPageFactory = false;

    protected $rebuysFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Forza\Rebuy\Model\RebuysFactory $rebuysFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->rebuysFactory = $rebuysFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        /*
        $rebuys = $this->rebuysFactory->create();
        $collection = $rebuys->getCollection();
        foreach($collection as $item){
            echo "<pre>";
            print_r($item->getData());
            echo "</pre>";
        }
        exit();
        */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Rebuys')));

        return $resultPage;
    }

    /**
     * Load rebuy list
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function indexAction() {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('core/template')->setTemplate('rebuy/quicksearch.phtml'));
        $this->_addContent($this->getLayout()->createBlock('rebuy/adminhtml_rebuy'));
        $this->renderLayout();
    }

    /**
     * Show rebuy dashboard
     */
    public function DashboardAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Create new rebuy in backend
     */
    public function newAction() {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('rebuy/adminhtml_rebuy_form_new'));
        $this->renderLayout();
    }

    /**
     * Show single rebuy
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function viewAction() {
        $id = $this->getRequest()->getParam('id');
        $model = $this->rebuyEntityRebuyFactory->create()->load($id);
        $this->registry->register('rebuy_data', $model);
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * ecport rebuy list to excel
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function exportExcelAction()
    {
        $fileName   = 'vaf.xls';
        $content    = $this->getLayout()->createBlock('forza_rebuy_block_adminhtml_rebuy_grid');

        $this->_prepareDownloadResponse($fileName, $content->getExcelFile());
    }

    /**
     * export rebuy list to CSV
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function exportCsvAction()
    {
        $fileName   = 'vaf.csv';
        $content    = $this->getLayout()->createBlock('forza_rebuy_block_adminhtml_rebuy_grid');

        $this->_prepareDownloadResponse($fileName, $content->getCsvFile());
    }

    /**
     * Edit rebuy address page
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function editAddressAction() {
        $id = $this->getRequest()->getParam('id');
        $model = $this->rebuyEntityRebuyFactory->create()->load($id);

        //set json price data to object
        $priceObj = json_decode($model->getPrice());
        $model->setPrice($priceObj);

        //set rebuy data to form
        if ($data = $this->backendSession->getFormData()) {
            $model->addData($data);
            $this->backendSession->setFormData(null);
        }

        $this->registry->register('rebuy_data', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('rebuy/adminhtml_rebuy_form_address'));
        $this->renderLayout();
    }

    /**
     * Save rebuy address data to existing or new rebuy
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function saveAction() {
        if ($id = $this->request->getParam('id')) {
            $model = $this->rebuyEntityRebuyFactory->create()->load($id);
        } else {
            $model = $this->rebuyEntityRebuyFactory->create();
        }

        $model->setFirstname($this->getRequest()->getParam('firstname'));
        $model->setLastname($this->getRequest()->getParam('lastname'));
        $model->setEmail($this->getRequest()->getParam('email'));
        $model->setTelephone($this->getRequest()->getParam('telephone'));
        $model->setCity($this->getRequest()->getParam('city'));
        $model->setPostalCode($this->getRequest()->getParam('postal_code'));
        $model->setStreet($this->getRequest()->getParam('street'));
        $model->setHousenumber($this->getRequest()->getParam('housenumber'));
        $model->setHousenumberAddition($this->getRequest()->getParam('housenumber_addition'));
        $model->setCountyCode($this->getRequest()->getParam('country_code'));
        $model->save();

        $this->generic->addSuccess(__('Rebuy saved'));
        return $this->_redirect('*/*/view',['id'=>$id]);
    }


    /**
     * add an internal comment to the rebuy
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addCommentAction() {
        $id = $this->request->getParam('id');

        if ($id && $this->request->getParam('comment')) {
            $model = $this->rebuyEntityRebuyCommentFactory->create();
            $model->setRebuyId($id);
            $model->setBody($this->request->getParam('comment'));
            $user = $this->backendAuthSession->getUser();
            $model->setBy($user->getFirstname().' '.$user->getLastname());
            $model->setCreatedAt(date('Y-m-d H:i:s'));
            $model->save();
            $this->generic->addSuccess(__('Comment added'));
        }

        return $this->_redirect('*/rebuy/view',['id'=>$id]);
    }


    /**
     * Load rebuy grid
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('Forza_Rebuy_Block_Adminhtml_Rebuy_Grid')->toHtml()
        );
    }


    /**
     * handle item processing
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function processItemsAction() {
        //check if rebuy id is not empty
        if ($rebuyId = $this->request->getParam('id')) {
            $rebuy = $this->rebuyEntityRebuyFactory->create()->load($rebuyId);
        }else{
            return $this->_redirect('*/*/index');
        }

        //create an array of all rebuy items
        $rebuyItems = [];
        foreach ($rebuy->getItems() as $item){
            $rebuyItems[$item->getId()] = $item;
        }

        //if items are set to received, save the received status
        $received = $this->request->getParam('received');
        if(!empty($received)){
            foreach ($received as $itemId => $isReceived){
                if($isReceived) {
                    $rebuyItems[$itemId]->setStatus('RECEIVED');
                    $rebuyItems[$itemId]->save();
                }
            }
        }

        //if imei is filled in save the imei, check if items are reported stolen.
        $imei = $this->request->getParam('imei');
        if(!empty($imei)){
            foreach ($imei as $itemId => $imeiCode){
                if($imeiCode == '') {
                    continue;
                }

                //Check if imei is reported stolen
                $check = Forza::getLibrary('DOR_FencingCheck');
                $check->setProduct($imeiCode, 'apple');

                //save the new status and imei to the items
                $rebuyItems[$itemId]->setImei($imeiCode);
                $rebuyItems[$itemId]->setFenchingStatus(($check->checkRegister()) ? 'REPORTED' : 'OK');
                $rebuyItems[$itemId]->setStatus('AWAITING_CHECK_RESULT');
                $rebuyItems[$itemId]->save();
            }

            //set rebuy to processing
            $rebuy->setReceiveDate(date('Y-m-d H:i:s'));
            $rebuy->setStatus('PROCESSING')->save();
        }

        //if item is marked as not received,  set item status to not received
        $notReceived = $this->request->getParam('not_received');
        if(!empty($notReceived)){
            foreach ($notReceived as $itemId => $isNotReceived){
                if($isNotReceived) {
                    $rebuyItems[$itemId]->setStatus('NOT_RECEIVED');
                    $rebuyItems[$itemId]->save();
                }
            }
        }

        //if received or not received is not empty, that means the rebuy has just been received. set rebuy to received and ens mail
        if(!empty($notReceived) || !empty($received)){
            $this->_sendEmail('rebuy_received' , $rebuy);
            $this->backendSession->addSuccess($this->__('Rebuy saved'));
            return $this->_redirect('*/*/view',['id'=>$rebuyId]);
        }

        //if rebuy is accepted by buyer, set the buyer price and seller price equal and save accepted status.
        $accepts = $this->request->getParam('accept');
        if(!empty($accepts)){
            foreach ($accepts as $itemId => $accept){
                if($accept) {
                    $rebuyItems[$itemId]->setBuyerPrice($rebuyItems[$itemId]->getSellerPrice());
                    $rebuyItems[$itemId]->setBuyerQuality($rebuyItems[$itemId]->getSellerQuality());
                    $rebuyItems[$itemId]->setStatus('ACCEPTED');
                    $rebuyItems[$itemId]->save();
                }
            }
        }

        //if item is declined by buyer, set the status to declined and save decline reason
        $declineList = $this->request->getParam('decline');
        $declineReason = $this->request->getParam('decline_reason');
        if(!empty($declineList)){
            foreach ($declineList as $itemId => $decline){
                if($decline) {
                    $rebuyItems[$itemId]->setStatus('DECLINED');
                    $rebuyItems[$itemId]->setReturnStatus('Recycle');
                    $rebuyItems[$itemId]->setBuyerPrice(0);
                    $rebuyItems[$itemId]->setBuyerNote($declineReason[$itemId]);
                    $rebuyItems[$itemId]->save();
                }
            }
        }

        //If product is different, change the product to the new, and save the old in history.
        $newProduct = $this->request->getParam('new_product');
        if(!empty($newProduct)){
            foreach ($newProduct as $itemId => $newProductData){
                if($newProductData['list'] != 0 ) {
                    /** @var \Forza\Rebuy\Model\Entity\RebuyProduct $product */
                    $product = $this->rebuyEntityRebuyProductFactory->create()->load($rebuyItems[$itemId]->getProductId());
                    $rebuyItems[$itemId]->setProductHistory($product->getName());
                    $rebuyItems[$itemId]->setPriceHistory($rebuyItems[$itemId]->getSellerPrice());
                    $rebuyItems[$itemId]->setProductId($newProductData['list']);
                    $rebuyItems[$itemId]->setNoteHistory($newProductData['reason']);
                    $product = $this->rebuyEntityRebuyProductFactory->create()->load($rebuyItems[$itemId]->getProductId());
                    $rebuyItems[$itemId]->setSellerPrice($product->getPriceByQuality($rebuyItems[$itemId]->getSellerQuality()));
                    $rebuyItems[$itemId]->save();
                }
            }
        }

        //if any product is changed,send new product email and save rebuy
        if(!empty($newProduct)){
            $this->_sendEmail('rebuy_product_change' , $rebuy);
            $this->backendSession->addSuccess($this->__('Rebuy saved'));
            return $this->_redirect('*/*/view',['id'=>$rebuyId]);
        }


        //if buyer has different bid, save the bid to the item.
        $bids = $this->request->getParam('bid');
        if(!empty($bids)){
            $bidCheck = [];
            foreach ($bids as $itemId => $bidData){
                if((int)$bidData['quality'] > 0) {
                    $product = $this->rebuyEntityRebuyProductFactory->create()->load($rebuyItems[$itemId]->getProductId());
                    if($bidData['quality'] <= 5 && $bidData['quality'] != 0){
                        $rebuyItems[$itemId]->setBuyerPrice($product->getPriceByQuality($bidData['quality']));
                        $rebuyItems[$itemId]->setBuyerQuality($bidData['quality']);
                        $rebuyItems[$itemId]->setBuyerNote($bidData['reason']);
                    }elseif ($bidData['quality'] == 6) {
                        $rebuyItems[$itemId]->setBuyerPrice($bidData['Custom bid']);
                        $rebuyItems[$itemId]->setBuyerQuality($bidData['quality']);
                        $rebuyItems[$itemId]->setBuyerNote($bidData['reason']);
                    }
                    $rebuyItems[$itemId]->setStatus('AWAITING_BID');
                    $rebuyItems[$itemId]->save();
                }
            }
        }

        //close order when order is declined by customer
        $closedOrder = $this->request->getParam('close_order');
        if(!empty($closedOrder)){
            foreach ($closedOrder as $itemId => $isClosed){
                if($isClosed) {
                    $rebuyItems[$itemId]->setStatus('CLOSED');
                    $rebuyItems[$itemId]->save();
                }
            }
        }


        if(count($rebuy->getItemsByStatus([ 'AWAITING_BID','DECLINED','AWAITING_CHECK_RESULT','RECEIVED'])) == 0){
            $rebuy->complete();
        }elseif(empty($bidCheck)){
            $rebuy->setStatus('AWAITING_SELLER')
                ->setBidExpire(date('Y-m-d H:i:s',strtotime('+5 days')))
                ->save();
            $this->_sendEmail('rebuy_bid', $rebuy);
        }
        $this->backendSession->addSuccess($this->__('Rebuy saved'));
        return $this->_redirect('*/*/view',['id'=>$rebuyId]);
    }

    public function invoiceAction() {
        $rebuyId = $this->request->getParam('id');
        $rebuy = $this->rebuyEntityRebuyFactory->create()->load($rebuyId);

        $pdf = $this->rebuyPdfInvoiceFactory->create()->setRebuy($rebuy)->getPdfFile();
        $name = $rebuy->getIncrementId() . '.pdf';
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }

    public function enquiryFormAction() {
        $rebuyId = $this->request->getParam('id');
        $rebuy = $this->rebuyEntityRebuyFactory->create()->load($rebuyId);

        $pdf = $this->rebuyPdfEnquiryFactory->create()->setRebuy($rebuy)->getPdfFile();
        $name = $rebuy->getIncrementId() . '.pdf';
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }


    public function productListAction() {
        $rebuyId = $this->request->getParam('id');
        $rebuy = $this->rebuyEntityRebuyFactory->create()->load($rebuyId);

        $pdf = $this->rebuyPdfListFactory->create()->setRebuy($rebuy)->getPdfFile();
        $name = $rebuy->getIncrementId() . '.pdf';
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }

    public function shipmentLabelAction() {
        $rebuyId = $this->request->getParam('id');
        $rebuy = $this->rebuyEntityRebuyFactory->create()->load($rebuyId);

        $pdfLabel = $this->rebuyServiceShipmentFactory->create()
            ->setRebuy($rebuy)
            ->getShipment()
            ->getLabel();

        $name = $rebuy->getIncrementId() . '_label.pdf';
        $this->_prepareDownloadResponse($name, $pdfLabel, 'application/pdf');
    }



    public function returnLabelAction(){
        $rebuyId = $this->request->getParam('id');
        $rebuy = $this->rebuyEntityRebuyFactory->create()->load($rebuyId);
        if(!$rebuy){
            return false;
        }
        $pdfLabel = $this->rebuyServiceReturnShipmentFactory->create()
            ->setRebuy($rebuy)
            ->getShipment()
            ->getLabel($_GET['label']);
        if($_GET['label'] == 'zpl'){
            $this->getResponse()->setHeader('Content-type', 'text/plain');
            return $this->getResponse()->setBody(base64_encode($pdfLabel));
        }

        $this->getResponse()->setHeader('Content-type', 'application/pdf');
        $this->getResponse()->setBody($pdfLabel);
    }

    public function cancelAction() {
        $rebuyId = $this->request->getParam('id');
        $rebuy = $this->rebuyEntityRebuyFactory->create()->load($rebuyId);
        $rebuy->close();
        return $this->_redirect('*/*/view',['id'=>$rebuyId]);

    }

    public function reopenAction()
    {
        if ($rebuyId = $this->request->getParam('id')) {
            $rebuy = $this->rebuyEntityRebuyFactory->create()->load($rebuyId);
        }else{
            return $this->_redirect('*/*/index');
        }

        $rebuy->setStatus('SHIPREADY');
        $rebuy->save();

        foreach($rebuy->getItems() as $item) {
            $item->setStatus('AWAITING_RECEIVE');
            $item->save();
        }
        return $this->_redirect('*/*/view',['id'=>$rebuyId]);

    }


    public function _sendEmail($type, $rebuy) {
        $storeId = 1;
        $emailTemplate  = $this->emailTemplateFactory->create()
            ->loadDefault($type);

        $vars = array(
            'rebuy' => $rebuy,
            'key' => md5($rebuy->getIncrementId().$rebuy->getLastname().$rebuy->getCreatedAt())
        );
        $emailTemplate->getProcessedTemplate($vars);
        $emailTemplate->setSenderEmail($this->scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId));
        $emailTemplate->setSenderName($this->scopeConfig->getValue('trans_email/ident_general/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId));
        $emailTemplate->send($rebuy->getEmail(),$rebuy->getFirstname().' '.$rebuy->getLastname(), $vars);

    }

    public function getUrlAction() {
        $rebuy = $this->rebuyEntityRebuyFactory->create()->load($_GET['id'], 'increment_id');
        if ($rebuy->getId() > 0) {
            $url = $this->backendHelper->getUrl('*/*/view/') . 'id/' . $rebuy->getId();
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(json_encode(['found' => true, 'url' => $url]));
        } else {
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(json_encode(['found' => false]));
        }
    }

    /*
    public function _isAllowed()
    {
        return $this->backendAuthSession->isAllowed('rebuy/rebuy');
    }

*/
}