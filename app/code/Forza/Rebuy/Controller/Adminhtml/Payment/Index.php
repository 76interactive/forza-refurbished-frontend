<?php
namespace Forza\Rebuy\Controller\Adminhtml\Payment;


class Index extends \Magento\Backend\App\Action {

    protected $resultPageFactory = false;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->registry = $registry;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->registry->register('rebuy_payment_grid_type','open',true);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Open Payments')));

        return $resultPage;
    }

    public function indexAction() {
        $this->loadLayout();
        $this->registry->register('rebuy_payment_grid_type','open',true);
        $this->_addContent($this->getLayout()->createBlock('rebuy/adminhtml_payment'));
        $this->renderLayout();
    }

    public function allAction() {
        $this->loadLayout();
        $this->registry->register('rebuy_payment_grid_type','all',true);
        $this->_addContent($this->getLayout()->createBlock('rebuy/adminhtml_payment'));
        $this->renderLayout();
    }

    public function completeAction() {
        if ($id = $this->request->getParam('id')) {
            $model = $this->rebuyEntityRebuyPaymentFactory->create()->load($id);
            if($model->getId() > 0){
                $model->setPaidAt(date('Y-m-d H:i:s'))
                    ->setStatus('PAID')
                    ->save();
            }
        }
        echo $this->backendHelper->getUrl('*/*/undo/') .'id/'.$id;die;
    }

    public function undoAction() {
        if ($id = $this->request->getParam('id')) {
            $model = $this->rebuyEntityRebuyPaymentFactory->create()->load($id);
            if($model->getId() > 0){
                $model->setPaidAt(NULL)
                    ->setStatus('AWAITING_PAYMENT')
                    ->save();
            }
        }
    }
    /*
    public function _isAllowed()
    {
        if($this->request->getActionName() == 'all') {
            return $this->backendAuthSession->isAllowed('rebuy/rebuy_all_payments');
        }else{
            return $this->backendAuthSession->isAllowed('rebuy/rebuy_payments');
        }
    }
    */
}