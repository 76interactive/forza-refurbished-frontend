<?php
namespace Forza\Rebuy\Controller\Adminhtml\Payment;


class All extends \Magento\Backend\App\Action {

    protected $resultPageFactory = false;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $this->registry->register('rebuy_payment_grid_type','all',true);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('All Payments')));

        return $resultPage;
    }
}