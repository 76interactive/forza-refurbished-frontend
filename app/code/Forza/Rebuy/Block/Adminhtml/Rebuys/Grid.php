<?php
namespace Forza\Rebuy\Block\Adminhtml\Rebuys;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $moduleManager;

    protected $rebuysFactory;

    protected $_exportFlag;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Forza\Rebuy\Model\RebuysFactory $rebuysFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->rebuysFactory = $rebuysFactory;
        $this->moduleManager = $moduleManager;

        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('gridGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('grid_record');

    }

    protected function _prepareCollection()
    {
        $collection = $this->rebuysFactory->create()->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    protected function _prepareColumns()
    {

        $this->addColumn('id', array(
            'header' => __('ID'),
            'sortable' => true,
            'width' => '60',
            'index' => 'id'
        ));

        $this->addColumn('increment_id', array(
            'header' => __('Rebuy') . ' #',
            'sortable' => true,
            'width' => '60',
            'index' => 'increment_id',
            'type' => 'text'
        ));

        $this->addColumn('firstname', array(
            'header' => __('Firstname'),
            'sortable' => true,
            'width' => '60',
            'index' => 'firstname',
            'type' => 'text'
        ));

        $this->addColumn('lastname', array(
            'header' => __('Lastname'),
            'sortable' => true,
            'width' => '60',
            'index' => 'lastname',
            'type' => 'text'
        ));

        $this->addColumn('city', array(
            'header' => __('City'),
            'sortable' => true,
            'width' => '60',
            'index' => 'city',
            'type' => 'text'
        ));

        $this->addColumn('postal_code', array(
            'header' => __('Postcode'),
            'sortable' => true,
            'width' => '60',
            'index' => 'postal_code',
            'type' => 'text'
        ));

        $this->addColumn('company_name', array(
            'header' => __('Company'),
            'sortable' => true,
            'width' => '60',
            'index' => 'company_name',
            'type' => 'text'
        ));

        $this->addColumn('status', array(
            'header' => __('Status'),
            'sortable' => true,
            'width' => '60',
            'index' => 'status',
            'type' => 'text'
        ));

        $this->addColumn('total', array(
            'header' => __('Total'),
            'sortable' => true,
            'width' => '60',
            'index' => 'total',
            'type' => 'price',
            'currency_code' => $this->storeManager->getStore()->load(1)->getBaseCurrency()->getCode(),

        ));
        $this->addColumn('created_at', array(
            'header' => __('Created at'),
            'sortable' => true,
            'width' => '60',
            'index' => 'created_at',
            'type' => 'datetime',
        ));


        if (!$this->_exportFlag) {

            $this->addColumn(
                'view',
                [
                    'header' => __('View'),
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => [
                        [
                            'caption' => __('View'),
                            'url' => [
                                'base' => '*/*/view'
                            ],
                            'field' => 'id'
                        ]
                    ],
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'header_css_class' => 'col-action',
                    'column_css_class' => 'col-action'
                ]
            );
        }

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }


    public function getRowUrl($row)
    {
        return $this->getUrl(
            '*/*/view',
            ['id' => $row->getId()]
        );
    }
}