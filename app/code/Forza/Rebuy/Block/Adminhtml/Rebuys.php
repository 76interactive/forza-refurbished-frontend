<?php
namespace Forza\Rebuy\Block\Adminhtml;


class Rebuys extends \Magento\Backend\Block\Widget\Container
{
    protected $_template = 'rebuys/grid.phtml';

    protected $rebuysFactory;

    public function __construct(\Magento\Backend\Block\Widget\Context $context)
    {
        $this->_controller = 'adminhtml_rebuys' ;
        $this->_blockGroup = 'Forza_Rebuy';
        $this->_headerText = __('Rebuys');
        $this->_addButtonLabel = __('Create New');

        parent::__construct($context);
    }

    protected function _prepareLayout()
    {

        $addButtonProps = [
            'id' => 'add_new_grid',
            'label' => __('Add New'),
            'class' => 'add',
            'button_class' => '',
            'class_name' => 'Magento\Backend\Block\Widget\Button\SplitButton',
            'options' => $this->_getAddButtonOptions(),
        ];
        $this->buttonList->add('add_new', $addButtonProps);

        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Forza\Rebuy\Block\Adminhtml\Rebuys\Grid', 'grid.view.grid')
        );
        return parent::_prepareLayout();
    }

    protected function _getAddButtonOptions()
    {

        $splitButtonOptions[] = [
            'label' => __('Add New'),
            'onclick' => "setLocation('" . $this->_getCreateUrl() . "')"
        ];
        return $splitButtonOptions;
    }


    protected function _getCreateUrl()
    {
        return $this->getUrl(
            '*/*/new'
        );
    }


    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }
}