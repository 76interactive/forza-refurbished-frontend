<?php

namespace Forza\Rebuy\Block\Adminhtml\Payment;


class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{

    const payment_status_awaiting = 'AWAITING_PAYMENT';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;


    protected $paymentFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $backendHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Forza\Rebuy\Model\PaymentFactory $paymentFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->registry = $registry;
        $this->paymentFactory = $paymentFactory;
        $this->resourceConnection = $resourceConnection;
        $this->backendHelper = $backendHelper;
        parent::__construct($context, $backendHelper, $data);
        $this->setId('order_items');
        $this->setDefaultSort('item_id');
    }


    protected function _prepareCollection()
    {
        if ($this->registry->registry('rebuy_payment_grid_type') == 'all') {
            //die("1");
            $collection = $this->getAllCollection();
        } else {
            //die("2");
            $collection = $this->getToPayCollection();
        }
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    public function getAllCollection()
    {
        $collection = $this->paymentFactory->create()->getCollection();
        $table = $this->resourceConnection->getTableName('rebuy');
        $collection->getSelect()->join(array('rebuy_table' => $table), '`main_table`.`rebuy_id` = `rebuy_table`.`id`', 'rebuy_table.increment_id');
        return $collection;
    }

    public function getToPayCollection()
    {
        $collection = $this->paymentFactory->create()->getCollection();
        $collection->addFieldToFilter('main_table.status', self::payment_status_awaiting);
        $table = $this->resourceConnection->getTableName('rebuy');
        $collection->getSelect()->join(array('rebuy_table' => $table), '`main_table`.`rebuy_id` = `rebuy_table`.`id`', 'rebuy_table.increment_id');
        return $collection;
    }


    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => __('ID'),
            'sortable' => true,
            'width' => '60',
            'index' => 'id'
        ));

        $this->addColumn('increment_id', array(
            'header' => __('Rebuy'),
            'sortable' => true,
            'width' => '60',
            'index' => 'increment_id',
            'type' => 'text'
        ));


        $this->addColumn('created_at', array(
            'header' => __('Date'),
            'sortable' => true,
            'width' => '30',
            'index' => 'created_at',
            'type' => 'datetime'
        ));

        $this->addColumn('method', array(
            'header' => __('Method'),
            'sortable' => true,
            'width' => '30',
            'index' => 'method',
            'type' => 'text'
        ));

        if ($this->registry->registry('rebuy_payment_grid_type') == 'all') {
            $this->addColumn('status', array(
                'header' => __('Status'),
                'sortable' => true,
                'width' => '30',
                'index' => 'status',
                'type' => 'text'
            ));

        }


        $this->addColumn('iban', array(
            'header' => __('IBAN'),
            'sortable' => true,
            'width' => '30',
            'index' => 'iban',
            'type' => 'text'
        ));

        $this->addColumn('iban_name', array(
            'header' => __('IBAN name'),
            'sortable' => true,
            'width' => '30',
            'index' => 'iban_name',
            'type' => 'text'
        ));

        if ($this->registry->registry('rebuy_payment_grid_type') == 'all') {
            $this->addColumn('paid_at', array(
                'header' => __('Paid at'),
                'sortable' => true,
                'width' => '30',
                'index' => 'paid_at',
                'type' => 'datetime'
            ));
        }

        $this->addColumn('amount', array(
            'header' => __('Total'),
            'sortable' => true,
            'width' => '30',
            //'renderer' => 'Forza_Rebuy_Block_Adminhtml_Render_Price',
            'index' => 'amount',
            'type' => 'price',
            'currency_code' => $this->storeManager->getStore()->load(1)->getBaseCurrency()->getCode(),
        ));

        if ($this->registry->registry('rebuy_payment_grid_type') == 'all') {
            $this->addColumn('tax', array(
                'header' => __('Tax'),
                'sortable' => true,
                'width' => '30',
                //'renderer' => 'Forza_Rebuy_Block_Adminhtml_Render_Price',
                'index' => 'tax',
                'type' => 'price'
            ));

            $this->addColumn('payment_cost', array(
                'header' => __('Payment cost'),
                'sortable' => true,
                'width' => '30',
                //'renderer' => 'Forza_Rebuy_Block_Adminhtml_Render_Price',
                'index' => 'payment_cost',
                'type' => 'price'
            ));
        }


        //$link = $this->backendHelper->getUrl('*/rebuy/view/') . 'id/$rebuy_id';
        /*
        $this->addColumn('action_edit', array(
            'header' => $this->helper('rebuy')->__('Action'),
            'width' => 15,
            'sortable' => false,
            'filter' => false,
            'type' => 'action',
            'actions' => array(
                array(
                    'url' => $link,
                    'caption' => $this->helper('catalog')->__('View'),
                ),
            )
        ));
        */
        if ($this->registry->registry('rebuy_payment_grid_type') == 'open') {
            //$link = $this->backendHelper->getUrl('*/*/complete/') . 'id/$id';
            /*
            $this->addColumn('delete_edit', array(
                'header' => $this->helper('rebuy')->__('Action'),
                'width' => 15,
                'sortable' => false,
                'filter' => false,
                'column_css_class' => 'mark_done_link',
                'type' => 'action',
                'actions' => array(
                    array(
                        'url' => $link,
                        'caption' => $this->helper('rebuy')->__('Mark as done'),
                    ),
                )
            ));
            */
        }

        return parent::_prepareColumns();
    }

    public function getRowUrl($item)
    {
        return $this->getUrl('*/rebuy/view', array('id' => $item->getRebuyId()));
    }
}