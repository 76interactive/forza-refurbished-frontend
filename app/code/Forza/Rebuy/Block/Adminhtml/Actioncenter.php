<?php
namespace Forza\Rebuy\Block\Adminhtml;


class Actioncenter extends \Magento\Backend\Block\Widget\Grid\Container
{
    public function __construct(\Magento\Backend\Block\Widget\Context $context)
    {
        $this->_controller = 'adminhtml_actioncenter' ;
        $this->_blockGroup = 'Forza_Rebuy';
        $this->_headerText = __('Actioncenter');
        $this->_addButtonLabel = __('Create New');

        parent::__construct($context);
    }
}