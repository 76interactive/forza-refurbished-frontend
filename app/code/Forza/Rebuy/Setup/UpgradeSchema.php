<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Forza\Rebuy\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {

            if (!$setup->tableExists($setup->getTable('rebuy_product'))) {
                // Create rebuy_product table
                $table = $setup->getConnection()->newTable(
                    $setup->getTable('rebuy_product')
                )->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    array(
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                    ),
                    'Id'
                )->addColumn(
                    'name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    array(
                        'nullable' => false,
                    ),
                    'name'
                )->addColumn(
                    'sku',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    array(
                        'nullable' => false,
                    ),
                    'sku'
                )->addColumn(
                    'type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    array(
                        'nullable' => false,
                    ),
                    'type'
                )->addColumn(
                    'memory',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    200,
                    array(
                        'nullable' => false,
                    ),
                    'memory'
                )->addColumn(
                    'connectivity',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    200,
                    array(
                        'nullable' => true,
                    ),
                    'connectivity'
                )->addColumn(
                    'price',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    200,
                    array(
                        'nullable' => false,
                    ),
                    'price'
                )->addColumn(
                    'active',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    array(
                        'nullable' => false,
                        'default' => 1,
                        'comment' => 'active'
                    ));

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy_quote_item'))) {
                // Create rebuy_quote_item table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy_quote_item'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                    ), 'Id')
                    ->addColumn('quote_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'quote id' => false,
                    ), 'name')
                    ->addColumn('product_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable' => false,
                    ), 'product id')
                    ->addColumn('quality', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable' => false,
                    ), 'quality')
                    ->addColumn('quantity', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable' => false,
                    ), 'quantity')
                    ->addColumn('price', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable' => false,
                    ), 'price')
                    ->addColumn('tax', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable' => false,
                    ), 'tax')
                    ->addColumn('payment_cost', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable' => false,
                    ), 'payment_cost');

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy_quote'))) {
                // Create rebuy_quote table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy_quote'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                    ), 'Id')
                    ->addColumn('customer_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable' => true,
                    ), 'customer_id')
                    ->addColumn('payment_method', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'payment_method' => true,
                    ), 'name')
                    ->addColumn('firstname', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'firstname')
                    ->addColumn('lastname', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'lastname')
                    ->addColumn('email', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'email')
                    ->addColumn('country_code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'country_code')
                    ->addColumn('street', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'street')
                    ->addColumn('housenumber', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'housenumber')
                    ->addColumn('housenumber_addition', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'housenumber_addition')
                    ->addColumn('postal_code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'postal_code')
                    ->addColumn('vat_number', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'vat_number')
                    ->addColumn('company_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'company_name')
                    ->addColumn('telephone', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'telephone')
                    ->addColumn('city', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'city')
                    ->addColumn('iban', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'iban')
                    ->addColumn('iban_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'iban_name');

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy'))) {
                // Create rebuy table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                    ), 'Id')
                    ->addColumn('increment_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'increment_id')
                    ->addColumn('customer_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable' => true,
                    ), 'customer_id')
                    ->addColumn('payment_method', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'payment_method' => false,
                    ), 'name')
                    ->addColumn('firstname', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'firstname')
                    ->addColumn('lastname', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'lastname')
                    ->addColumn('email', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'email')
                    ->addColumn('country_code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'country_code')
                    ->addColumn('street', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'street')
                    ->addColumn('housenumber', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'housenumber')
                    ->addColumn('housenumber_addition', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'housenumber_addition')
                    ->addColumn('postal_code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'postal_code')
                    ->addColumn('vat_number', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'vat_number')
                    ->addColumn('company_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'company_name')
                    ->addColumn('telephone', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'telephone')
                    ->addColumn('total', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable' => false,
                    ), 'total')
                    ->addColumn('tax', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable' => false,
                    ), 'tax')
                    ->addColumn('payment_cost', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable' => false,
                    ), 'payment_cost')
                    ->addColumn('status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'status')
                    ->addColumn('shipment_expire', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable' => true,
                    ), 'shipment_expire')
                    ->addColumn('bid_expire', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable' => true,
                    ), 'bid_expire')
                    ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable' => false,
                    ), 'created_at')
                    ->addColumn('city', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => true,
                    ), 'city')
                    ->addColumn('receive_date', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable' => true,
                    ), 'receive_date');

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy_item'))) {
                // Create rebuy_item table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy_item'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity'  => true,
                        'unsigned'  => true,
                        'nullable'  => false,
                        'primary'   => true,
                    ), 'Id')
                    ->addColumn('rebuy_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'quote id'  => false,
                    ), 'rebuy_id')
                    ->addColumn('product_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable'  => false,
                    ), 'product id')
                    ->addColumn('seller_quality', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable'  => false,
                    ), 'seller_quality')
                    ->addColumn('seller_price', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable'  => false,
                    ), 'seller_price')
                    ->addColumn('buyer_quality', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable'  => true,
                    ), 'buyer_quality')
                    ->addColumn('buyer_price', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable'  => true,
                    ), 'buyer_price')
                    ->addColumn('status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => false,
                    ), 'status')
                    ->addColumn('buyer_note', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, array(
                        'nullable'  => true,
                    ), 'buyer_note')
                    ->addColumn('tax', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable'  => false,
                    ), 'tax')
                    ->addColumn('payment_cost', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable'  => false,
                    ), 'payment_cost')
                    ->addColumn('imei', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => true,
                    ), 'imei')
                    ->addColumn('fenching_status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => true,
                    ), 'fenching_status')
                    ->addColumn('return_status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => true,
                    ), 'return_status')
                    ->addColumn('product_history', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, array(
                        'nullable'  => true,
                    ), 'product_history')
                    ->addColumn('price_history', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable'  => true,
                    ), 'price_history')
                    ->addColumn('note_history', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, array(
                        'nullable'  => true,
                    ), 'note_history');

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy_payment'))) {
                // Create rebuy_payment table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy_payment'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity'  => true,
                        'unsigned'  => true,
                        'nullable'  => false,
                        'primary'   => true,
                    ), 'Id')
                    ->addColumn('rebuy_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'quote id'  => false,
                    ), 'rebuy_id')
                    ->addColumn('amount', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable'  => false,
                    ), 'amount')
                    ->addColumn('status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => false,
                    ), 'status')
                    ->addColumn('method', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => false,
                    ), 'method')
                    ->addColumn('iban', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => true,
                    ), 'iban')
                    ->addColumn('iban_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => true,
                    ), 'iban_name')
                    ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable'  => true,
                    ), 'created_at')
                    ->addColumn('paid_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable'  => true,
                    ), 'paid_at')
                    ->addColumn('tax', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable'  => true,
                    ), 'tax')
                    ->addColumn('payment_cost', \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT, null, array(
                        'nullable'  => true,
                    ), 'payment_cost');

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy_shipment'))) {
                // Create rebuy_shipment table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy_shipment'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity'  => true,
                        'unsigned'  => true,
                        'nullable'  => false,
                        'primary'   => true,
                    ), 'Id')
                    ->addColumn('rebuy_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'quote id'  => false,
                    ), 'rebuy_id')
                    ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable'  => false,
                    ), 'created_at')
                    ->addColumn('tracking_number', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => false,
                    ), 'tracking_number')
                    ->addColumn('label_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => false,
                    ), 'label_id');

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy_comment'))) {
                // Create rebuy_comment table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy_comment'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity'  => true,
                        'unsigned'  => true,
                        'nullable'  => false,
                        'primary'   => true,
                    ), 'Id')
                    ->addColumn('rebuy_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'quote id'  => false,
                    ), 'rebuy_id')
                    ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable'  => false,
                    ), 'created_at')
                    ->addColumn('by', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable'  => false,
                    ), 'by')
                    ->addColumn('body', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, array(
                        'nullable'  => false,
                    ), 'body');

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy_action'))) {
                // Create rebuy_action table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy_action'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                    ), 'Id')
                    ->addColumn('rebuy_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'quote id' => false,
                    ), 'rebuy_id')
                    ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(
                        'nullable' => false,
                    ), 'created_at')
                    ->addColumn('by', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'by')
                    ->addColumn('type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, array(
                        'nullable' => false,
                    ), 'type')
                    ->addColumn('body', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, array(
                        'nullable' => false,
                    ), 'body')
                    ->addColumn('is_done', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'default' => 0,
                        'nullable' => false,
                    ), 'is_done');

                $setup->getConnection()->createTable($table);
            }

            if (!$setup->tableExists($setup->getTable('rebuy_tag'))) {
                // Create rebuy_tag table
                $table = $setup->getConnection()
                    ->newTable($setup->getTable('rebuy_tag'))
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                    ), 'Id')
                    ->addColumn('tag_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'tag' => false,
                    ), 'rebuy_id')
                    ->addColumn('rebuy_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                        'nullable' => false,
                    ), 'rebuy_id');

                $setup->getConnection()->createTable($table);
            }
        }
        
        $setup->endSetup();
    }
}
