<?php

namespace Forza\Vat\Config\Source;

class AttributeSet
{

    protected $attributeSet;

    public function __construct(\Magento\Catalog\Model\Product\AttributeSet\Options $attributeSet)
    {
        $this->attributeSet = $attributeSet;
    }

    /**
     * Get list of available attribute sets
     *
     * @return array
     */
    public function toOptionArray()
    {
        $attributeSets = Mage::getModel("eav/entity_attribute_set")->getCollection();
        $attributeSetList = array();
        foreach ($attributeSets as $attributeSet){
            $attributeSetList[] = array('value' => $attributeSet->getAttributeSetId(), 'label' => $attributeSet->getAttributeSetName());
        }
        return $attributeSetList;
    }


}