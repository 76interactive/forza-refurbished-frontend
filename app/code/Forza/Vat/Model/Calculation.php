<?php

namespace Forza\Vat\Model;

use Magento\Store\Model\ScopeInterface;

class Calculation
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get an config array of attribute sets that are marked as refurbished
     * @return array
     */
    public function getRefurbishedAttributeSets()
    {
        return explode(',', $this->scopeConfig->getValue('vat/general/attribute_set', ScopeInterface::SCOPE_STORE));
        //return [10];
        return explode(',',Mage::getStoreConfig('forza_vat/settings/attribute_set'));
    }

    /**
     * Calculate the total vat free of the refurbished articles
     * @param $quote
     * @return float|int
     */
    public function getRefurbishedAmount($quote)
    {
        $totalPrice = 0;
        $products = $this->getRefurbishedItems($quote);
        foreach ($products as $productData){
            list($product,$qty) = $productData;
            $totalPrice += $this->getProductPriceWithoutTax($product,$qty);
        }
        return $totalPrice;
    }

    /**
     * Get the product price without tax
     * @param $product
     * @param $qty
     * @return float|int
     */
    public function getProductPriceWithoutTax($product, $qty)
    {
        return ($product->getPrice() *  $qty);
    }

    /**
     * Get all refurbished items in a quote
     * @param $quote
     * @return array
     */
    public function getRefurbishedItems($quote)
    {
        $refurbishedProducts = array();
        $cartProducts = $quote->getAllItems();
        foreach ($cartProducts as $item) {
            if (!in_array($item->getProduct()->getAttributeSetId(), $this->getRefurbishedAttributeSets())) {
                continue;
            }
            $refurbishedProducts[] = array($item,$item->getQty());
        }

        return $refurbishedProducts;
    }

}

