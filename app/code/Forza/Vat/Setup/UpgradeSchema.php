<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Forza\Vat\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            if ($setup->tableExists($setup->getTable('quote'))) {
                if ($connection->tableColumnExists($setup->getTable('quote'), 'vat_shifted') === false) {
                    $setup->getConnection()->addColumn(
                        $setup->getTable('quote'),
                        'vat_shifted',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            'comment' => 'Vat Shifted'
                        ]
                    );
                }
            }

            if ($setup->tableExists($setup->getTable('quote_item'))) {
                if ($connection->tableColumnExists($setup->getTable('quote_item'), 'shifted_vat') === false) {
                    $setup->getConnection()->addColumn(
                        $setup->getTable('quote_item'),
                        'shifted_vat',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 64,
                            'comment' => 'Shifted Vat'
                        ]
                    );
                }
            }

            if ($setup->tableExists($setup->getTable('sales_order'))) {
                if ($connection->tableColumnExists($setup->getTable('sales_order'), 'vat_shifted') === false) {
                    $setup->getConnection()->addColumn(
                        $setup->getTable('sales_order'),
                        'vat_shifted',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            'comment' => 'Vat Shifted'
                        ]
                    );
                }
            }

        }
        $setup->endSetup();
    }
}
