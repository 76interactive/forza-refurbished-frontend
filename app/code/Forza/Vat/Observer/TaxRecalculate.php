<?php

namespace Forza\Vat\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

class TaxRecalculate implements ObserverInterface
{

    protected $amountLimit;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Forza\Vat\Model\Calculation
     */
    protected $calculation;

    /**
     * @var \Magento\Tax\Helper\Data
     */
    protected $taxHelper;

    /**
     * TaxRecalculate constructor.
     * @param \Forza\Vat\Model\Calculation $calculation
     * @param \Magento\Tax\Helper\Data $taxHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Forza\Vat\Model\Calculation $calculation,
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->calculation = $calculation;
        $this->taxHelper = $taxHelper;
        $this->scopeConfig = $scopeConfig;

        $this->amountLimit = $this->scopeConfig->getValue('vat/general/amount_limit', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param Observer $observer
     * @return bool|void
     */
    public function execute(Observer $observer)
    {
        $quote = $observer->getQuote();

        if (!$this->scopeConfig->getValue('vat/general/enable', ScopeInterface::SCOPE_STORE)) {
            return;
        }

        if ($quote->getSubtotal() < $this->amountLimit) {
            return;
        }

        if ($this->calculation->getRefurbishedAmount($quote) < $this->amountLimit) {
            //$quote->setTotalsCollectedFlag(false)->collectTotals();
            //$quote->save();
            // Add logic in-case product was removed after validating VAT and vat_shifted is saved
        } else {
            $this->recalculateTax($quote);
        }
    }

    /**
     * modify quote to tax free if, tax free is allowed
     * @param $quote
     * @return mixed
     */
    protected function recalculateTax($quote){

        $taxFree = $this->taxHelper->isCrossBorderTradeEnabled();

        //$validVat = Mage::getModel('tax/config')->isValidVat();
        if(!$taxFree){
            return $quote;
        }
        $quote->setData('vat_shifted', 1);
        $quote->save();

        $this->quoteSetRefurbishedVatFree($quote);
    }

    /**
     * Change the price of the items to tax free
     * @param $quote
     * @return mixed
     */
    public function quoteSetRefurbishedVatFree($quote)
    {
        $shiftedVatAmount = $this->removeVatFromItems($quote->getAllVisibleItems());
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress->getTaxAmount()- $shiftedVatAmount  > 0 ) {
            $shippingAddress->setTaxAmount($shippingAddress->getTaxAmount()- $shiftedVatAmount);
            $shippingAddress->setBaseTaxAmount($shippingAddress->getBaseTaxAmount() - $shiftedVatAmount);
        } else {
            $shippingAddress->setTaxAmount(0);
            $shippingAddress->setBaseTaxAmount(0);
        }
        $shippingAddress->setSubtotalInclTax($shippingAddress->getSubtotalInclTax() - $shiftedVatAmount);
        $shippingAddress->setGrandTotal($shippingAddress->getGrandTotal() - $shiftedVatAmount);
        $shippingAddress->setBaseGrandTotal($shippingAddress->getBaseGrandTotal() - $shiftedVatAmount);
        $this->removeVatFromItems($shippingAddress->getAllVisibleItems());
    }

    /**
     * Remove VAT from items
     * @param $items
     * @return int
     */
    public function removeVatFromItems($items)
    {
        $refurbishedAttributeSet = $this->calculation->getRefurbishedAttributeSets();
        $shiftedVatAmount = 0;
        foreach ($items as $item) {
            if (!in_array($item->getProduct()->getAttributeSetId(), $refurbishedAttributeSet)) {
                continue;
            }

            if ($item->getShiftedVat()) {
                $shiftedVatAmount += $item->getShiftedVat();
            } else {
                $shiftedVatAmount += $item->getTaxAmount();
                $item->setShiftedVat($item->getTaxAmount());
            }

            $item->setTaxPercent(0);
            $item->setTaxAmount(0);
            $item->setBaseTaxAmount(0);
            $item->setPriceInclTax($item->getPrice());
            $item->setBasePriceInclTax($item->getPrice());
            $item->setRowTotalInclTax($item->getRowTotal());
            $item->setBaseRowTotalInclTax($item->getBaseRowTotal());
            $item->getProduct()->setTaxClassId(0);
            $item->save();
        }

        return $shiftedVatAmount;
    }
}